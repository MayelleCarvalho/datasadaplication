import { Questionario } from "src/model/questionario";

export class Item {
    _id: number;
    descricao: string;
    questionario: Questionario;
}