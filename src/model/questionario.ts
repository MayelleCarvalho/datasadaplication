import { Perfil } from "src/model/perfil";

export class Questionario {
    id: number;
    descricao: string;
    autor: Perfil;
}