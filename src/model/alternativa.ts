import { Item } from "src/model/item";

export class Alternativa {
    _id: number;
    descricao: string;
    peso: number;
    item: Item;
    marked: boolean;
}