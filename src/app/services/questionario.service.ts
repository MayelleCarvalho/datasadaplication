import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Perfil } from 'src/model/perfil';

@Injectable()

export class QuestionarioService {

  private apiRoot =  'http://127.0.0.1:8000/';

  constructor(private http: HttpClient) { }

  getQuestionarios(){
    return this.http.get(this.apiRoot.concat('questionario/'));
  }

  createQuestionario(descricao: string, autor: Perfil){
    return this.http.post(
      this.apiRoot.concat('questionario/'),
      { descricao, autor }
    );

  }

  deleteQuestionario(id: number){
    return this.http.delete(this.apiRoot.concat(`questionario/${id}/`));
  }

  detailQuestionario(id: number){
    return this.http.get(this.apiRoot.concat(`questionario/${id}/`));
  }

}
