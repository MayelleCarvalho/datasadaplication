import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { catchError, tap, map } from 'rxjs/operators';
import { Perfil } from 'src/model/perfil';

@Injectable()
export class PerfilService {

  private apiRoot =  'http://127.0.0.1:8000/';

  constructor(private http: HttpClient) { }

  getPerfis(){
    return this.http.get(this.apiRoot.concat('usuario/'));
  }

  createPerfil(username: string, last_name: string, first_name:string, email: string, senha: string){
    return this.http.post(
      this.apiRoot.concat('usuario/'),
      { username, last_name, first_name, email, senha }
    );

  }

  deletePerfil(id: number){
    return this.http.delete(this.apiRoot.concat(`usuario/${id}/`));
  }

  detailPerfil(id: number){
    return this.http.get(this.apiRoot.concat(`usuario/${id}/`));
  }

  /*
  private extractData(res: Response) {
    let body = res;
    return body || { };
  }

  getPerfis(): Observable<any> {
    return this.httpClient.get(this.apiUrl + 'usuarios').pipe(
      map(this.extractData)
    );
  }*/

}
