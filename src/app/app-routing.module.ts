import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PerfilComponent } from './perfil/perfil.component';
import { QuestionarioComponent } from './questionario/questionario.component';
import { ItemComponent } from './item/item.component';
import { AlternativaComponent } from './alternativa/alternativa.component';

const routes: Routes = [
  {
    path: '', redirectTo: 'perfis', pathMatch: 'full'
  },
  {
    path : 'perfil',
    component: PerfilComponent,
    data: { title: 'Lista de Perfis'}
  },
  {
    path : 'questionario',
    component: QuestionarioComponent,
    data: { title: 'Lista de Questionarios'}
  },
  {
    path : 'item',
    component: ItemComponent,
    data: { title: 'Lista de Itens'}
  },
  {
    path : 'alternativa',
    component: AlternativaComponent,
    data: { title: 'Lista de Alternativas'}
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
