import { Component, OnInit } from '@angular/core';
import { Questionario } from 'src/model/questionario';
import { QuestionarioService } from '../services/questionario.service';
import { Perfil } from 'src/model/perfil';

@Component({
  selector: 'app-questionario',
  templateUrl: './questionario.component.html',
  styleUrls: ['./questionario.component.css']
})
export class QuestionarioComponent implements OnInit {

  questionarios: Questionario[];
  error: any;

  constructor(private _api:QuestionarioService) { }

  ngOnInit() {
    this._api.getQuestionarios().subscribe(
      (questionarios: Questionario[]) => this.questionarios = questionarios,
      (error: any) => this.error = error
    );
  }

  delete(id: number){
    this._api.deleteQuestionario(id).subscribe(
      (sucess:any) => this.questionarios.splice(
        this.questionarios.findIndex(questionario => questionario.id == id)
      )
    );
  }
  add(descricaoQuestionario: string, autorQuestionario: Perfil){
    this._api.createQuestionario(descricaoQuestionario, autorQuestionario).subscribe(
      (questionario: Questionario) => this.questionarios.push(questionario)
    );
  }

}
