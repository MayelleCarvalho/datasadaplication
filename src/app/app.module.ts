import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PerfilComponent } from './perfil/perfil.component';
import { QuestionarioComponent } from './questionario/questionario.component';
import { ItemComponent } from './item/item.component';
import { AlternativaComponent } from './alternativa/alternativa.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MenuComponent } from './menu/menu.component';
import { RouterModule, Routes } from '@angular/router';
import {  
  MatButtonModule,
  MatCardModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatProgressSpinnerModule,
  MatSelectModule,
  MatSidenavModule,
  MatTableModule,
  MatToolbarModule } from '@angular/material';
import { HttpClientModule } from '@angular/common/http';
import { LayoutModule } from '@angular/cdk/layout';
import { PerfilDetailComponent } from './perfil-detail/perfil-detail.component';
import { PerfilNovoComponent } from './perfil-novo/perfil-novo.component';
import { PerfilEditarComponent } from './perfil-editar/perfil-editar.component';
import { PerfilService } from './services/perfil.service';
import { QuestionarioService } from './services/questionario.service';

const routes: Routes = [
  {
    path: 'perfil',
    component: PerfilComponent,
    data : { title: 'Lista de Perfis'}
  },
  {
    path: 'perfil-detalhe/:id',
    component: PerfilDetailComponent,
    data : { title: 'Detalhe de Perfil'}
  },
  {
    path: 'questionario',
    component: QuestionarioComponent,
    data : { title: 'Lista de Questionários'}
  }
];

@NgModule({
  declarations: [
    AppComponent,
    PerfilComponent,
    QuestionarioComponent,
    ItemComponent,
    AlternativaComponent,
    MenuComponent,
    PerfilDetailComponent,
    PerfilNovoComponent,
    PerfilEditarComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,  
    MatButtonModule,
    MatInputModule,
    MatCardModule,
    MatIconModule,
    MatListModule,
    MatProgressSpinnerModule, 
    MatSelectModule,
    MatSidenavModule,  
    MatTableModule,
    MatToolbarModule,
    LayoutModule
  ],
  providers: [
    PerfilService,
    QuestionarioService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
