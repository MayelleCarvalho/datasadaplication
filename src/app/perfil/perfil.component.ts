import { Component, OnInit } from '@angular/core';
import { PerfilService } from '../services/perfil.service';
import { Perfil } from 'src/model/perfil';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.css']
})
export class PerfilComponent implements OnInit {
  
  perfis: Perfil[];
  error: any;

  constructor(private _api:PerfilService) { }

  ngOnInit() {
    this._api.getPerfis().subscribe(
      (perfis: Perfil[]) => this.perfis = perfis,
      (error: any) => this.error = error
    );
  }

  delete(id: number){
    this._api.deletePerfil(id).subscribe(
      (sucess:any) => this.perfis.splice(
        this.perfis.findIndex(perfil => perfil.id == id)
      )
    );
  }
  add(perfilUsername: string, perfilFirstName:string, perfilLastName: string, perfilEmail:string, perfilSenha: string){
    this._api.createPerfil(perfilUsername, perfilFirstName, perfilLastName, perfilEmail, perfilSenha).subscribe(
      (perfil: Perfil) => this.perfis.push(perfil)
    );
  }

}
